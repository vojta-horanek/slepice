using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Slepice
{
    class Program
    {

        private static Dictionary<string, Action<List<string>>> commands = new Dictionary<string, Action<List<string>>>()
        {
            { "add", _add },
            { "show", _show },
            { "no", _delete },
            { "set", _edit },
            { "export", _export },
            { "import", _import },
            { "exit", _exit },
            { "help", _help }
        };

        private static List<Chicken> db = new List<Chicken>();

        private static bool run = true;


        private const string PS1 = "chicken@linux:~$ ";

        static void Main(string[] args)
        {
            SystemdBoot();
            PrintLogo();
            bash();
        }

        private static void SystemdBoot()
        {
            Console.WriteLine("Started version 243.17");
            Console.WriteLine("/dev/sda1 clean: 1561681681/7816816 files, 19849161681/18181618 blocks");
            Console.WriteLine("[  OK  ] Started Load/Save Random Seed");
            Console.WriteLine("[  OK  ] Activated swap on /dev/sda2");
            Console.WriteLine("[  OK  ] Reached target shell");
            Console.WriteLine("[  OK  ] Auto login on getty@tty1");
            Console.WriteLine("[  OK  ] Started Chicken shell");
            Console.WriteLine();
        }

        private static void PrintLogo()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(
                "     __//\n" +
                "    /.__.\\\n"+
                "    \\ \\/ /\n"+
                " \'__/    \\\n"+
                "  \\-      )\n"+
                "   \\_____/\n"+
                "_____|_|____\n"
            );
            Console.ResetColor();
        }


        static void bash()
        {

            while (run)
            {

                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(PS1);
                Console.ResetColor();

                var stdin = Console.ReadLine().Trim();

                if (stdin == "")
                {
                    continue;
                }

                if (!commands.ContainsKey(stdin.Split(' ')[0]))
                {
                    Console.WriteLine("bash: " + stdin.Split(' ')[0] + ": command not found");
                    continue;
                }

                
                foreach (var command in commands)
                {
                    if (command.Key != stdin.Split(' ')[0]) continue;

                    var args = stdin.Split(' ').ToList();
                    args.RemoveAt(0);
                    command.Value(args);
                    break;
                }
            }

        }

        private static string PrintChicken(Chicken chicken, bool index = false, int i = 0)
        {
            if (index)
            {
                return ("[" + i + "]: " + chicken.name + ": " + chicken.average + "\n");
            }
            else
            {
                return (chicken.name + ": " + chicken.average + "\n");
            }
        }

        static void _add(List<string> argv)
        {
            if (argv.Count != 2)
            {
                Console.WriteLine("add: usage: add [name] [average]");
                return;;
            }

            float avg;
            if(!float.TryParse(argv[1], out avg))
            {
                Console.WriteLine("add: " + argv[1] + ": not a valid average");
                return;
            }

            db.Add(new Chicken(argv[0], avg));
        }

        static void _show(List<string> argv)
        {
            if (argv.Count == 1)
            {
                int foundCount = 0;
                for (var i = 0; i < db.Count; i++) {
                    if (db[i].name != argv[0]) continue;
                    Console.Write(PrintChicken(db[i], true, i));
                    foundCount++;
                }
                if (foundCount == 0)
                    Console.WriteLine("show: " + argv[0] + ": name not found");
                return;
            }

            Console.Write(GetShow());

        }

        static string GetShow()
        {
            string retVal = "";
            for (var i = 0; i < db.Count; i++)
            {
                retVal += PrintChicken(db[i], true, i);
            }
            return retVal;
        }


        static void _delete(List<string> argv)
        {
            if (argv.Count != 1)
            {
                Console.WriteLine("no: usage: no [id]");
                return;
            }

            int id;
            if (!int.TryParse(argv[0], out id))
            {
                Console.WriteLine("no: " + argv[0] + ": not a valid id");
                return;
            }

            if (id >= db.Count || id < 0)
            {
                Console.WriteLine("no: " + argv[0] + ": not a valid id");
                return;
            }
            
            db.RemoveAt(id);
        }
        static void _edit(List<string> argv)
        {
            if (argv.Count != 3)
            {
                Console.WriteLine("set: usage: set [id] [name] [average]");
                return;
            }

            int id;
            if (!int.TryParse(argv[0], out id))
            {
                Console.WriteLine("set: " + argv[0] + ": not a valid id");
                return;
            }

            if (id >= db.Count || id < 0)
            {
                Console.WriteLine("set: " + argv[0] + ": not a valid id");
                return;
            }

            float avg;
            if (!float.TryParse(argv[2], out avg))
            {
                Console.WriteLine("set: " + argv[1] + ": not a valid average");
                return;
            }

            db[id] = new Chicken(argv[1], avg);

        }
        static void _export(List<string> argv)
        {
            if (argv.Count != 2)
            {
                Console.WriteLine("export: usage: export [format] [file]");
                Console.WriteLine("available formats: \"json\", \"xml\", \"binary\"\nif invalid format is specified, then the show command output is used");
                return;
            }

            if (argv[0] == "binary")
            {
                using (FileStream streamOut = new FileStream(argv[1], FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(streamOut, db);
                }
            }
            else
            {
                string output;

                switch (argv[0])
                {
                    case "json":
                        output = JsonConvert.SerializeObject(db);
                        break;
                    case "xml":
                        output = new XElement("Chickens", db.Select(i => 
                                new XElement("Chicken", 
                                    new XElement("Name", i.name), 
                                    new XElement("Average", i.average)
                                    )
                        )).ToString();
                        break;
                    default:
                            output = GetShow();
                        break;
                }

                File.WriteAllText(argv[1], output);
            }
        }

        static void _import(List<string> argv)
        {
            if (argv.Count != 1)
            {
                Console.WriteLine("import: usage: import [file]");
                return;
            }

            using (FileStream streamIn = File.OpenRead(argv[0]))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                db = (List<Chicken>)formatter.Deserialize(streamIn);
            }
        }
        static void _exit(List<string> argv)
        {
            run = false;
        }
        static void _help(List<string> argv)
        {
            Console.WriteLine("available commands:");
            foreach (var command in commands)
            {
                Console.WriteLine(command.Key);
            }
        }
    }
}
