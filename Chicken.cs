using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slepice
{
    [Serializable]
    class Chicken
    {
        public float average;
        public string name;

        public Chicken(string name, float average)
        {
            this.name = name;
            this.average = average;
        }
    }
}
